package edu.salber.java.avance;

import java.util.concurrent.locks.ReentrantLock;

class SharedResource {
    private int count = 0;
    

    public  void increment() {
		count++;	
        
    }

    public  int getCount() {
        return count;
    }
}

class IncrementThread extends Thread {
    private SharedResource resource;

    public IncrementThread(SharedResource resource) {
        this.resource = resource;
    }

    public void run() {
        for (int i = 0; i < 1000; i++) {
            resource.increment();
        }
    }
}

public class Main5 {
    public static void main(String[] args) throws InterruptedException {
        SharedResource resource = new SharedResource();
        IncrementThread thread1 = new IncrementThread(resource);
        IncrementThread thread2 = new IncrementThread(resource);

        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        System.out.println("R�sultat final : "+resource.getCount());
    }
}
