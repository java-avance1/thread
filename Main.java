package edu.salber.java.avance.app;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Main {
	private static int NUB_PRODUCERS = 10;
	private static int NUM_LINE = 10;
	private static String FILE_NAME= "./data1.txt";
	
	public static void main(String[] args) throws InterruptedException, IOException {
        QueueManager queueManager = new QueueManager(3); // Capacit� de la file d'attente : 3
        Consumer consumer = new Consumer(queueManager, "./data1.txt");
        consumer.start();
        List<Thread> listThreads = new ArrayList<>();
        for(int i=0;i<NUB_PRODUCERS;i++) {
        	Producer producer = new Producer(queueManager,  NUM_LINE);
        	producer.start();
        	listThreads.add(producer);
        }
        listThreads.forEach( t -> {
			try {
				t.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
        Thread.sleep(1000);
        consumer.interrupt();
    	try (Stream<String> fileStream = Files.lines(Paths.get(FILE_NAME))) {
    		//Lines count
    		int noOfLines = (int) fileStream.count();
    		System.out.println("NOMBRE TOTAL DE LIGNE : "+noOfLines);
    	}        
	}
}
