package edu.salber.java.avance.app;

import java.util.LinkedList;
import java.util.Queue;

public class QueueManager {
    private Queue<String> queue = new LinkedList<>();
    private final int capacity;

    public QueueManager(int capacity) {
        this.capacity = capacity;
    }

    public synchronized void enqueue(String item) throws InterruptedException {
        while (queue.size() == capacity) {
            System.out.println("File d'attente pleine. " + Thread.currentThread().getName() + " attend...");
            // Attendre jusqu'� ce qu'il y ait de l'espace dans la file d'attente
        }
        queue.add(item);
        System.out.println(item + " a �t� ajout� � la file d'attente.");
         // R�veiller un thread en attente
    }

    public synchronized String dequeue() throws InterruptedException {
        while (queue.isEmpty()) {
            System.out.println("File d'attente vide. " + Thread.currentThread().getName() + " attend...");
            wait(); // Attendre jusqu'� ce qu'il y ait des �l�ments dans la file d'attente
        }
        String item = queue.poll();
        System.out.println(item + " a �t� retir� de la file d'attente.");
        // R�veiller un thread en attente
        return item;
    }
}
